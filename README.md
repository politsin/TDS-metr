# ТЗ - кондуктометр (TDS Metr)

## Задача:
- Определение концентрации растворенных веществ в воде на основе её проводимости
- Предлагается процессор STM32G031K8T6, но там всего `8kBytes of SRAM` незнаю достаточно-ли, можно заменить. Если памяти хватает в притык, то точно не стоит упарываться над оптимизациями и точно стоит заменить, т.к. функционал будет увеличен.
- Пины в CubeMX https://gitlab.com/politsin/TDS-metr/-/blob/main/stm32-G031K8T6-board.ioc 
- Распиновка картинкой https://gitlab.com/politsin/TDS-metr/-/blob/main/TDS-распиновка.png
- Внеший вид картинкой https://gitlab.com/politsin/TDS-metr/-/blob/main/TDS-внешний-вид.png
- Схема https://gitlab.com/politsin/TDS-metr/-/blob/main/TDS-схема.png

## Вид:
- Модуль который является слейвом по UART/i2c
- При установке дисплея сам может показывать посчитанную цифру

## Состоит из:
- NTC - термистор для определения температуры воды
- Ds18b20 - для ещё одной температуры нужен на этапе R&D
- TDS-probe - "2 гвоздя в воде" между которыми измеряем падение напряжения
- дисплей tm1637 (7-сегментный на 4 символа)  для отображения результата

## Принцип работы:
- "Очистка" с частотой 25kHz
  - `+2500mV` --[1000Ω]--  --[TDS-probe]-- `GND`
  - `GND` --[1000Ω]--  --[TDS-probe]-- `+2500mV`
- "Измерение" падение напряжения в одну и другую стророну чтобы исключить поляризацию
  - `+2500mV` --[1000Ω]-- АЦП --[TDS-probe]-- `GND`
  - `GND` --[1000Ω]-- АЦП --[TDS-probe]-- `+2500mV`
- Считаем EC (электропроводимость в миллисименсах) по формуле
- Считаем NTC (температуру в миллиградусах)
  - на этапе R&D электропроводимость считаем без температурной компенсации

## Дополнительно:
- Информацию о EC и температуре показываем на дисплее tm1637 поочереди
- Отдаём информацию по UART/i2c если просят
- Конфиругируем устройство по UART/i2c

## Полное описание функционала:
- 1. удалили
- 2. GPIO_PIN_9 `DS18B20_Pin` Получать данные с датчика ds18b20
  - период опроса - раз в 5(*) минут
  - сохраняем в глобальную переменную если данные валидны
  - (можно объединить с АЦП [п3] если не хватает таймеров)
- 3. Получение данных с АЦП о температуре и напряжении и сохранение 8 значений в кольцевых буферах (глобальные переменные)
  - GPIO_PIN_2 `ADC_REF_Pin` - напряжение с опорного источника
  - GPIO_PIN_1 `ADC_NTC_Pin` - термистор (подать `+` на `PWR_NTC_Pin`)
  - внутренний источник опорного напряжения
  - V-bat напряжение питания
  - период опроса - раз в 5(*) минут
- 4. Кондуктометр в режиме "очистка" (Clear):
  -  `PWR_EC_Pin` - подать `+` чтобы подать напряжение на драйвер
  -  `EC1_Pin` и `EC2_Pin` в режиме `Output Compare1 CH1 CH1N`
  -  Частота работы 5-100kHz(*) задаётся в настройках
- 5. Кондуктометр в режиме "измерение" (Measure):
  -  `PWR_EC_Pin` - подать `+` чтобы подать напряжение на драйвер
  -  `EC1_Pin` и `EC2_Pin` в режиме `Output Compare1 CH1 CH1N`
  -  `ADC_EC_Pin` и `ADC_ECN_Pin` 2 пина АЦП подключены в одно место, можно использовать один или оба.
  -  Режим работы. 16 раз повторяем чтобы заполнить 2 массива :
     - `EC1_Pin=ON` `EC2_Pin=OFF` 
     - Задержка (config.ecDelay в тиках не менее 200 тиков было на f103)
     - АЦП1[] - делаем 16 измерений (или 1 в кольцевой буфер) 
     - `EC1_Pin=OFF` `EC2_Pin=ON` 
     - Задержка (config.ecDelay в тиках)
     - АЦП2[] - делаем 16 измерений (или 1 в кольцевой буфер)
  - * в низу документа код того как это тестировалось на stm32f103c8t6
- 6. Кондуктометр в режиме "простой" (Idle):
  - `PWR_EC_Pin` - подать `GND` чтобы отключить драйвер
- 7. Режим работы кондукторметра:
  - "очистка" (Clear) - длительность в настройках, по умолчанию 100мс
  - "измерение" (Measure) - полный цикл для заполнения 2х массивов по 16
  - "простой" (Idle) - длительность от 0 до 10 минут задаётся в настройках
- 8. Подсчёт данных (App) - сырые АЦП пересчитываем конечные значения каждые 10 минут(*appDelay):
  - 8.1 NTC ("%.2f") (uint, °mC) - считаем в милиградусах, округлённых до 10
    - После того как посчитали - отправляем на дисплей tm1637 (п11)
  - 8.2 V-REF (uint, mV) - переводим в миливольты
  - 8.3 V-REF-Internal (uint, mV) - переводим в миливольты
  - 8.4 EC-POSITIVE = среднее АЦП1 (uint, mV) переведённое в миливольты
  - 8.5 EC-NEGATIVE = config.referenceVoltage - среднее АЦП2 (uint, mV)
  - 8.6 EC-DELTA = EC-POSITIVE - EC-NEGATIVE; (int, mV)
  - 8.7 EC-RAW = (EC-POSITIVE + EC-NEGATIVE)/2 (uint, mV)
  - 8.8 EC (uint, mS - микросименсы, 100-10000) - считаем по формуле:
     - uint32_t Uref = config.referenceVoltage;
     - uint32_t Ro = config.ecRo;
     - uint32_t Ro = config.ecRo;
     - uint32_t R = EC-RAW * Ro / (Uref - EC-RAW);
     - uint32_t ec = config.ecKoefA / (R - config.ecKoefB) - config.ecKoefC;
     - После того как посчитали - отправляем на дисплей tm1637 (п11)
- 9. Получение данных:
  - UART, 115200 (PA2, PA3):
    - Получаем всё из п.8
    - Конфигурируем всё из п.10
  - [вероятнее всего не делаем] i2c-Slave (PA11, PA12):
    - Получаем NTC: NTC uint в миллицельсиях
    - Получаем АЦП: V-REF, V-REF-Internal
    - Получаем EC: EC-POSITIVE, EC-NEGATIVE, EC-RAW, EC
    - Конфигурируем NTC: ntcR1, ntcRo, ntcKoefB
    - Конфигурируем EC: ecRo, ecKoefA, ecKoefB, ecKoefC, ecKoefT
    - (можно и всё получать и конфигурировать если это просто)
- 10. Конфигурирование:
    - Предполагаю команды конфигурирования по UART давать вида `+CFG:ntcR1=10000`
  ```
  // Общее:
  uint32_t referenceVoltage; // 2500mv (напряжение питания датчиков)
  // NTC (параметры для высчитывания температуры по термистору)
  uint32_t ntcR1;    // 10kΩ voltage divider resistor value
  uint32_t ntcRo;    // 10kΩ R of Thermistor at 25 degree
  uint32_t ntcTo;    // 25 Temperature in Kelvin for 25 degree
  uint32_t ntcKoefB; // 3950 Beta value
  // EC (параметры для высчитывания EC у кондукторметра)
  int32_t ecRo;     //  Voltage divider resistor value 500Ω / 1000Ω
  int32_t ecKoefA;  //  Alfa value
  int32_t ecKoefB;  //  Beta value
  int32_t ecKoefC;  //  С-value
  int32_t ecKoefT;  //  Koef Temperature
  // Управление состоянием (помеченные как *):
  uint32_t appDelay;        // Задержка подсчета данных п.8 (10м)
  uint32_t ds18b20Delay;    // Задержка опроса ds18b20 (5 минут)
  uint32_t adcDelay;        // Задержка опроса ацп (5 минут)
  uint32_t ecClearHz;       // Частота для режима очистки (30kHz)
  uint32_t ecClearDuration; // Длительность очистки перед измерением
  uint32_t ecDelay;         // Длительность в тиках между переключением и запуском АЦП
  uint32_t ecIdle;          // Длительность простоя 0-10 минут (100ms)
  ```
- 11. дисплей tm1637 `X_SDA_Pin` и `X_SCL_Pin`
  показываем по очедеди температуру и посчитанные EC раз в секунду
  
## HAL
```
- [ ] GPIO_PIN_6 `LED_Pin`     / `LED_GPIO_Port`
- [ ] GPIO_PIN_2 `ADC_REF_Pin` / `ADC_REF_GPIO_Port` 
- [ ] GPIO_PIN_0 `PWR_NTC_Pin` / `PWR_NTC_GPIO_Port`
- [ ] GPIO_PIN_1 `ADC_NTC_Pin` / `ADC_NTC_GPIO_Port`
- [ ] GPIO_PIN_1 `PWR_EC_Pin`  / `PWR_EC_GPIO_Port`
- [ ] GPIO_PIN_7 `EC1_Pin`     / `EC1_GPIO_Port` 
- [ ] GPIO_PIN_8 `EC2_Pin`     / `EC2_GPIO_Port`
- [ ] GPIO_PIN_5 `ADC_EC_Pin`  / `ADC_EC_GPIO_Port`
- [ ] GPIO_PIN_6 `ADC_ECN_Pin` / `ADC_ECN_GPIO_Port`
- [ ] GPIO_PIN_9 `DS18B20_Pin` / `DS18B20_GPIO_Port` 
- [ ] GPIO_PIN_4 `ENC1_Pin`    / `ENC1_GPIO_Port` 
- [ ] GPIO_PIN_5 `ENCB_Pin`    / `ENCB_GPIO_Port` 
- [ ] GPIO_PIN_6 `ENC2_Pin`    / `ENC2_GPIO_Port` 
- [ ] GPIO_PIN_7 `X_SDA_Pin`   / `X_SDA_GPIO_Port`
- [ ] GPIO_PIN_8 `X_SCL_Pin`   / `X_SCL_GPIO_Port`
```

### main.h который получился после `CubeMX`
```c
#define DS18B20_Pin GPIO_PIN_9
#define DS18B20_GPIO_Port GPIOB
#define RESET_Pin GPIO_PIN_2
#define RESET_GPIO_Port GPIOF
#define PWR_NTC_Pin GPIO_PIN_0
#define PWR_NTC_GPIO_Port GPIOA
#define PWR_EC_Pin GPIO_PIN_1
#define PWR_EC_GPIO_Port GPIOA
#define RTC_ALARM_Pin GPIO_PIN_4
#define RTC_ALARM_GPIO_Port GPIOA
#define ADC_EC_Pin GPIO_PIN_5
#define ADC_EC_GPIO_Port GPIOA
#define ADC_ECN_Pin GPIO_PIN_6
#define ADC_ECN_GPIO_Port GPIOA
#define EC1_Pin GPIO_PIN_7
#define EC1_GPIO_Port GPIOA
#define ADC_NTC_Pin GPIO_PIN_1
#define ADC_NTC_GPIO_Port GPIOB
#define ADC_REF_Pin GPIO_PIN_2
#define ADC_REF_GPIO_Port GPIOB
#define EC2_Pin GPIO_PIN_8
#define EC2_GPIO_Port GPIOA
#define LED_Pin GPIO_PIN_6
#define LED_GPIO_Port GPIOC
#define T_JTMS_Pin GPIO_PIN_13
#define T_JTMS_GPIO_Port GPIOA
#define T_JTCK_Pin GPIO_PIN_14
#define T_JTCK_GPIO_Port GPIOA
#define ENC1_Pin GPIO_PIN_4
#define ENC1_GPIO_Port GPIOB
#define ENCB_Pin GPIO_PIN_5
#define ENCB_GPIO_Port GPIOB
#define ENC2_Pin GPIO_PIN_6
#define ENC2_GPIO_Port GPIOB
#define X_SDA_Pin GPIO_PIN_7
#define X_SDA_GPIO_Port GPIOB
#define X_SCL_Pin GPIO_PIN_8
#define X_SCL_GPIO_Port GPIOB
```

## Как это тестировалось на stm32f103c8t6 (bluepill)
На блюпил делал измерения с частотой `10kHz`
Настройки https://gitlab.com/politsin/TDS-metr/-/blob/main/TDS-stm32f103c8t6-example.png

- 200 тиков задержка нужно чтобы всё точно переключилось.
- 2500 тиков нужно чтобы DMA наполнило 16 ячеек АЦП и усреднило данные.
- 72MHz у нас частота stm32f103, 26666*Hz частота замера АЦП. За это время мы померяем половину - положительное или отриацтельное приложение напряжения.
- В итоге поставил таймер на 3600 тиков чтобы с учётом аналогичных вычислений видеть на экране "осцилографа" красивые 10kHz


```c
  // Комплиментарные.
  HAL_TIM_OC_Start_IT(&htim1, TIM_CHANNEL_1);
  HAL_TIMEx_OCN_Start_IT(&htim1, TIM_CHANNEL_1);
  __HAL_TIM_ENABLE_IT(&htim1, TIM_IT_UPDATE);
```
```c
/**
  * @brief This function handles TIM1 update interrupt.
  */
void TIM1_UP_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_IRQn 0 */
  // После задержки запускаем АЦП
  HAL_ADC_Start_DMA(&hadc1, (uint32_t *)&adc, EC_ADC_SIZE);
  /* USER CODE END TIM1_UP_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_UP_IRQn 1 */

  /* USER CODE END TIM1_UP_IRQn 1 */
}
/**
  * @brief This function handles TIM1 capture compare interrupt.
  */
void TIM1_CC_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_CC_IRQn 0 */
  if (ecMode == 1) {
    ecMode = 0;
  } else {
    ecMode = 1;
  }
  /* USER CODE END TIM1_CC_IRQn 0 */
  HAL_TIM_IRQHandler(&htim1);
  /* USER CODE BEGIN TIM1_CC_IRQn 1 */

  /* USER CODE END TIM1_CC_IRQn 1 */
}
/**
  * @brief This function handles DMA1 channel1 global interrupt.
  */
void DMA1_Channel1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel1_IRQn 0 */
  adcSum = 0;
  for (uint16_t i = 0; i < adcSize; i++) {
    adcSum = adcSum + adc[i];
  }
  if (ecMode == 1) {
    adcEc1 = HAL_ADC_GetValue(&hadc1);
    adcEc1 = (adcSum >> adcOversampling);
  } else {
    adcEc2 = HAL_ADC_GetValue(&hadc1);
    adcEc2 = (adcSum >> adcOversampling);
  }
  /* USER CODE END DMA1_Channel1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_adc1);
  /* USER CODE BEGIN DMA1_Channel1_IRQn 1 */

  /* USER CODE END DMA1_Channel1_IRQn 1 */
}
```

```c

 /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 3600;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 2500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_SET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */
```

```c++
// Analog Raw To String.
int32_t rawNtcToTemperature(uint32_t Vout) {
  uint32_t Rt;
  double kelvin, celsius;
  uint32_t R1 = ecConfig.getNtcR1(); // 9600  | voltage divider resistor value
  uint32_t Ro = ecConfig.getNtcRo(); // 10000 | R of Thermistor at 25 degree
  uint32_t To = ecConfig.getNtcTo(); // 25 | Temperature in Kelvin for 25 degree
  uint32_t koefB = ecConfig.getNtcKoefB(); // 3950  | Beta value
  uint32_t mv = ecConfig.getReferenceVoltage() - Vout;
  // uint32_t mv = (uint32_t)temperature_fir.filter((double)Vout);
  /*
   * Use Steinhart equation (simplified B parameter equation) to convert
   * resistance to kelvin B param eq: T = 1/( 1/To + 1/B * ln(R/Ro) ) T =
   * Temperature in Kelvin R = Resistance measured Ro = Resistance at
   * nominal temperature B = Coefficent of the thermistor To = Nominal
   * temperature in kelvin
   */
  Rt = R1 * mv / (ecConfig.getReferenceVoltage() - mv);
  kelvin = (double)Ro / (double)Rt;              // R/Ro
  kelvin = log(kelvin);                          // ln(R/Ro)
  kelvin = (1 / (double)koefB) * kelvin;         // 1/B * ln(R/Ro)
  kelvin = (1 / ((double)To + 273.15)) + kelvin; // 1/To + 1/B * ln(R/Ro)
  kelvin = 1 / kelvin; // 1/( 1/To + 1/B * ln(R/Ro) )​
  // Convert Kelvin to Celsius.
  celsius = kelvin - 273.15;
  return round(celsius * 1000);
}
```
